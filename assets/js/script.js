$(document).ready(function(){
	// !background image ----------------------------------------------
	 $.backstretch("http://iremhouston.org/assets/img/elements/body-bg.jpg");

	// !main nav ----------------------------------------------
	$('.nav_1, .icon_1').hover(
		function(){
			$('.icon_1').css({'backgroundPosition': '50% -224px'});
			$('.nav_1').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_1').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_1').css({'backgroundPosition': '50% 0px'});
				$('.nav_1').css({'color':'#57585b'});
			}
		}
	);
	$('.nav_2, .icon_2').hover(
		function(){
			$('.icon_2').css({'backgroundPosition': '50% -253px'});
			$('.nav_2').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_2').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_2').css({'backgroundPosition': '50% -29px'});
				$('.nav_2').css({'color':'#57585b'});
			}
		}
	);
	$('.nav_11, .icon_11').hover(
		function(){
			$('.icon_11').css({'backgroundPosition': '50% -280px'});
			$('.nav_11').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_11').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_11').css({'backgroundPosition': '50% -56px'});
				$('.nav_11').css({'color':'#57585b'});
			}
		}
	);
	$('.nav_12, .icon_12').hover(
		function(){
			$('.icon_12').css({'backgroundPosition': '50% -308px'});
			$('.nav_12').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_12').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_12').css({'backgroundPosition': '50% -84px'});
				$('.nav_12').css({'color':'#57585b'});
			}
		}
	);
	$('.nav_16, .icon_16').hover(
		function(){
			$('.icon_16').css({'backgroundPosition': '50% -336px'});
			$('.nav_16').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_16').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_16').css({'backgroundPosition': '50% -112px'});
				$('.nav_16').css({'color':'#57585b'});
			}
		}
	);
	$('.nav_19, .icon_19').hover(
		function(){
			$('.icon_19').css({'backgroundPosition': '50% -364px'});
			$('.nav_19').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_19').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_19').css({'backgroundPosition': '50% -140px'});
				$('.nav_19').css({'color':'#57585b'});
			}
		}
	);
	$('.nav_24, .icon_24').hover(
		function(){
			$('.icon_24').css({'backgroundPosition': '50% -393px'});
			$('.nav_24').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_24').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_24').css({'backgroundPosition': '50% -169px'});
				$('.nav_24').css({'color':'#57585b'});
			}
		}
	);
	$('.nav_25, .icon_25').hover(
		function(){
			$('.icon_25').css({'backgroundPosition': '50% -420px'});
			$('.nav_25').css({'color':'#0668b3'});
		},
		function(){
			var parent_class = $('.nav_25').parent().attr('class');
			if(parent_class === 'active'){
				return;
			}else{
				$('.icon_25').css({'backgroundPosition': '50% -196px'});
				$('.nav_25').css({'color':'#57585b'});
			}
		}
	);

	// !career & IYP ----------------------------------------------
	$(window).hashchange(function(e){
		e.preventDefault();
		var hash = location.hash;
		if(hash === '#resumes'){
			$('#job_link').parent('li').removeClass('active');
			$('#resume_link').parent('li').addClass('active');
			$('#resumes').css({'display':'block'});
			$('#job_posts').css({'display':'none'});
		}else if(hash === '#job_posts'){
			$('#resume_link').parent('li').removeClass('active');
			$('#job_link').parent('li').addClass('active');
			$('#resumes').css({'display':'none'});
			$('#job_posts').css({'display':'block'});
		}else if(hash === '#iyp'){
			var iyp = $("#iyp").animate({backgroundColor: '#0668b3', color: '#fffff'});
		  setTimeout(function() {
		   	iyp.animate({backgroundColor: '#ffffff', color: '#57585B'});
			}, 700);
		}else{return;}
	});
	$("#job_link").click(function(e){
		e.preventDefault();
		window.location.hash = "#job_posts";
	});
	$("#resume_link").click(function(e){
		e.preventDefault();
		window.location.hash = "#resumes";
	});
	$(window).trigger('hashchange');

	// !team ----------------------------------------------
	$('.anchor_nav').localScroll({hash:false});

	// !upcoming events----------------------------
	$('#next_event > .calendar_details').slice(1).remove();
	$('#next_event').show();
	$('#upcoming_events > .calendar_details').slice(3).remove();
	$('#upcoming_events').show();

   // !testimonial slideshow----------------------------
    $('.slideshow img').hide(); // hides all images
    $('.slideshow img:first').show(); // shows the first image
		$(window).load(function() {
		  $('.slideshow img').show(); // hides all images
		  $('.slideshow').cycle({
		    speed:  4000,
		    cleartype:  true,
	   		cleartypeNoBg:  true,
		  });
		});

	// !guests events----------------------------
  $('.event_price_group').click(function(){
	  $('#attend_quantity_label_single').hide();
		$('#attend_quantity_label_multiple').show();
	});
	$('.non_group_radio').click(function(){
		$('#attend_quantity_label_multiple').hide();
		$('#attend_quantity_label_single').show();
	});

	// !events registration----------------------------
	$('#open_registration').toggle(function(){
  	$('#registration_form').show(500);
  	$('#open_registration').text('Cancel').css('backgroundColor', '#0668b3');
	}, function(){
  	$('#registration_form').hide(500);
  	$('#open_registration').text('Register').css('backgroundColor', '#4db787');
	});


	// !courses form validation----------------------------
	var course_body_id = $('body').hasClass('type-6');
	if(course_body_id === true){
		gc_validation = function() {
		  if (!$('input[name=price]:radio:checked').val()) {
		  	alert('Please select an option.');
		  	return false;
		  }else{
				return true;
			}
		}
		fcc.events.cart.preprocess.add(gc_validation);
	}

	// !pay invoice form validation----------------------------
	var invoice_body_id = $('body').hasClass('page-177');
	if(invoice_body_id === true){
		gc_validation = function() {
			var inv_errors = 0;
		  if (!$('#in_price').val()) {
		  	inv_errors ++;
		  	$('#price_label').addClass('required');

		  	var p_val = $('#in_price').val();
		  	console.log('p_val: '+p_val);
		  	console.log('p_errors: '+inv_errors);
		  }else{
			  $('#price_label').removeClass('required');
		  }
		  if(!$('#in_number').val()){
			  inv_errors ++;
		  	$('#invoice_label').addClass('required');

		  	var in_val = $('#in_number').val();
		  	console.log('in_val: '+in_val);
		  	console.log('in_errors: '+inv_errors);
		  }else{
			  $('#invoice_label').removeClass('required');
		  }
		  if(!$('#in_name').val()){
			  inv_errors ++;
		  	$('#name_label').addClass('required');

		  	var n_val = $('#in_name').val();
		  	console.log('in_val: '+n_val);
		  	console.log('n_errors: '+inv_errors);
		  }else{
			  $('#full_name').removeClass('required');
		  }
			if (inv_errors > 0) {
				alert('Please complete all the required fields.');
				return false;
			}else {
				return true;
			}
		}
		fcc.events.cart.preprocess.add(gc_validation);
	}

	// !events form validation----------------------------
	var event_body_id = $('body').hasClass('type-24');
	if(event_body_id === true){
		gc_validation = function() {
		  var errors = 0;
	  	if(!$('input[name=price]:radio:checked').val()){
			  errors ++;
			  $('input[name=price]:radio').next().addClass('error');
		  }else{
			  $('input[name=price]:radio').next().removeClass('error');
		  }
		  if (!$('#attend_quantity').val()) {
				errors ++;
		  	$('#attend_quantity').next().addClass('error');
			}else{
				$('#attend_quantity').next().removeClass('error');
			}
			if (!$('#attend_name').val()) {
				errors ++;
		    $('#attend_name').next().addClass('error');
			}else{
				$('#attend_name').next().removeClass('error');
			}
			if (errors > 0) {
				alert('Please complete all the required fields in blue.');
				return false;
			}else {
				return true;
			}
		}
	  fcc.events.cart.preprocess.add(gc_validation);
	 }


 //Friend Search functionality

  var options = {
    valueNames: ['tags','company']
  };

  var userList = new List('friend_search', options);

  var categoryArray = [];
  var length = $('#friend_search .friend').length;
  $('#friend_search .friend').each(function(){
    var category = $(this).attr('data-category');
    categoryArray.push(category);
  });
  categoryArray.sort();
  function removeDups(categoryArray) {
    var result = [], map = {}, item;
    for (var i = 0; i < categoryArray.length; i++) {
        item = categoryArray[i];
        if (!map[item]) {
            result.push(item);
            map[item] = true;
        }
    }
    return(result);
  }
  var uniqueCategoryArray = removeDups(categoryArray);
  var i;
  $(uniqueCategoryArray).each(function(index, item) {
    $("#category_picker").append($('<option id="'+item+'">'+item+'</option>'));
  });
  $("#category_picker option[id='']").html("Uncategorized");

  $('#search_input').on('input', function() {
      console.log('Text1 changed!');
  });

 $(function() {
    $("#category_picker").change(function() {
      if ($("#all_categories").is(":selected")) {
        $('.friend').removeClass('active_category').show();
      }else{
        var category = $('#category_picker option:selected').attr('id');
        $('.friend[data-category="'+category+'"]').addClass('active_category').show();
        $('.friend:not([data-category="'+category+'"])').removeClass('active_category').hide();
      }
    }).trigger('change');
  });

 $('.selectpicker').selectpicker();


}); // close document.ready()