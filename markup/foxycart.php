<?php $roger_irem_api_key = x1vsX5JFdtwz1azzRJ3ffxEmA1GfSe7f5sBSmcnuvPYqTnTAYMF8efqE6WPN ?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <link rel="shortcut icon" href="../assets/img/favicon.png" type="image/x-icon" />
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="stylesheet" href="../assets/css/less.css">
  <script src="../assets/js/libs/modernizr-2.0.6.min.js"></script>
  <script src="../assets/js/libs/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/checkbox.js"></script>
  <script type="text/javascript" src="http://use.typekit.com/cwd2qfr.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  
<!-- BEGIN FOXYCART FILES -->
<link rel="stylesheet" href="//cdn.foxycart.com/static/scripts/colorbox/1.3.18/style1_fc/colorbox.css" type="text/css" media="screen" charset="utf-8" />
<script src="//cdn.foxycart.com/iremhouston/foxycart.colorbox.js" type="text/javascript" charset="utf-8"></script>
<!-- END FOXYCART FILES -->

</head>
<body class="page-[[*id]]">
<?php
require_once('verification.php');
ob_start();
?>
  <div id="container">
    <header>
    	
    </header>
    <div id="main" class="container_12" role="main">
    		<form action="https://iremhouston.foxycart.com/cart" method="post" accept-charset="utf-8">
						<fieldset>
							<input type="hidden" name="name" value="Some Product" />
							<input type="hidden" name="code" value="product1" />
							<p><strong>Registration Fees:</strong></p>
							<div class="controls">
								<input name="price" type="radio" id="course_price_premier" class="styled" value="10" />
								<p>Premier Rate:</strong> $10<br />
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
								
								<input name="price" type="radio" id="course_price_classic" class="styled" value="20" />
								<p>Classic Rate:</strong> $20<br />
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
								
								<input name="price" type="radio" id="course_price_nonmember" class="styled" value="30" />
								<p>Nonmember Rate:</strong> $30<br />
								Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
							</div><!-- /.controls -->
							<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
							<input class="register_link" name="my_submit" value="Register" type="submit"   />
						</fieldset>
					</form>

    </div><!-- /#main -->
    <footer>
			
  	</footer>
  </div> <!--! /#container -->

  <script defer src="../assets/js/plugins.js"></script>
  <script defer src="../assets/js/script.js"></script>
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  <?php
	$output = ob_get_contents();
	ob_end_clean();
	echo FoxyCart_Helper::fc_hash_html($output);
	?>
</body>
</html>
