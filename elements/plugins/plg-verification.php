<?php
/**
 * FoxyCart Validation Plugin for MODx 2.x (Revolution)
 *
 * @author Bryan Levay at Darkstar Design // bryan@darkstardesign.com // http://www.darkstardesign.com
 * @version 0.1
 * @example  http://wiki.foxycart.com/docs/cart/validation
 * 
 *
 * Requirements:
 *   - Form "code" values should not have leading or trailing whitespace.
 *   - Cannot use double-pipes in an input's name
 *   - Empty textareas are assumed to be "open"
 */
 
if (empty($results)) $results = array();
 
switch ($modx->event->name) {
	case "OnWebPagePrerender":
  $validate = $modx->getOption('base_path').'assets/plugins/foxycart/verification.php';
		if (file_exists($validate)) {
			include_once($validate);
			$doc = $modx->resource->_output;
			$modx->resource->_output = FoxyCart_Helper::fc_hash_html($doc);
		}
		break;
 
	default:  // stop here
		return;
		break;
}
?>