<?php
if (!function_exists('multiarray_keys')) {
    function multiarray_keys($ar) {
        foreach ($ar as $k => $v) {
            $keys[] = $k;
            if (is_array($ar[$k]))
                $keys = array_merge($keys, multiarray_keys($ar[$k]));
        }
        return $keys;
    }
}
$parents = (!empty($parents) || $parents === '0') ? explode(',', $parents) : array($modx->resource->get('id'));
$depth = isset($depth) ? (integer) $depth : 10;
 
$tree = $modx->getTree($parents, $depth);
$tree = multiarray_keys($tree);
$tree = implode(',', $tree);
$tree = 'FIELD(modResource.id, ' . $tree . ')';
return $tree;
?>